import React from 'react';

const Button: React.FC<React.ButtonHTMLAttributes<HTMLButtonElement>> = ({ children, ...props }) => {
  return (
    <button
      className="px-6 shadow-xl text-xl py-3 bg-purple-800 hover:bg-purple-600 border-purple-800 text-white rounded-md shadow-md hover:bg-dark-brownish focus:outline-none"
      {...props}
    >
      {children}
    </button>
  );
};

export default Button;