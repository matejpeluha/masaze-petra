import React from 'react';

const AboutMe: React.FC = () => {
  return (
    <div className="py-10 bg-light-brownish">
      <div className="container mx-auto flex items-center justify-center">
        <div className="text-center">
          <div className='flex flex-row justify-center'><img src="petra.jpeg" alt="me" className="rounded-full w-32 h-30 mb-4" /></div>
          <h2 className="text-3xl font-semibold mb-4">O mne</h2>
          <p className="text-lg max-w-[610px]">
            Moje meno je <b>Petra</b> a po zmene pracovného zamerania som sa s vášňou pustila do masérskeho remesla.
            Absolvovala som <b>akreditovaný</b> vzdelávací masérsky kurz s <b>certifikátom</b> na ktorom mám zdokladovaných <b>320 hodín</b> praxe aj vzdelávania v oblasti fyziológie človeka.
            Na trhu sa nachádza veľa masérov bez certifikátov a vzdelania, ktorí vám možu poškodiť zdravie. 
            Vďaka intenzívnemu akreditovanému kurzu vám dokážem pomôcť s vašimi problémami a dokonale sa uvoľniť.
            Pre vaše maximálne pohodlie <b>prídem až ku vám domov</b> s vlastným masérskym stolom. 
           </p>
        </div>
      </div>
    </div>
  );
};

export default AboutMe;