import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEnvelope } from '@fortawesome/free-regular-svg-icons';
import { faWhatsapp } from '@fortawesome/free-brands-svg-icons';
import DownloadLocalFile from './DownloadLocalFileButton';


const Contact: React.FC = () => {
  return (
    <div id="contact-section" className="pb-10 text-black text-center flex flex-col items-center">
      <h2 className="text-3xl font-semibold mb-4">Kontakt</h2>
      <div className='w-fit max-w-[340px] flex flex-col text-left gap-2'>
        <p className='flex flex-row justify-center items-center'>
            <a href="mailto:petapeluhova@gmail.com" className="grow inline-block py-2 px-4 bg-purple-600 text-white rounded transition duration-300 hover:bg-purple-800">
            <FontAwesomeIcon icon={faEnvelope} className="mr-2 justify-center items-center" /> petapeluhova@gmail.com
            </a>
        </p>
        <p className='flex flex-row justify-center items-center'>
            <a href="tel:+421902909006" className="grow inline-block py-2 px-4 bg-purple-600 text-white rounded transition duration-300 hover:bg-purple-800">
            <FontAwesomeIcon icon={faWhatsapp} className="mr-2 justify-center items-center" /> +421902909006
            </a>
        </p>
        <p className="text-lg text-center max-w-[600px] pt-4">
            <b>Neváhajte ma kontaktovať!</b>
           </p>
           <DownloadLocalFile />
      </div>
    </div>
  );
};

export default Contact;