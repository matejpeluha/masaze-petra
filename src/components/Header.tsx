import React from 'react';
import Button from './Button'

const Header: React.FC<{ scrollToContact: () => void }> = ({ scrollToContact }) => {
    const introStyles = {
        backgroundImage: "url('intro.jpg')",
        backgroundSize: 'cover',
        backgroundPosition: 'center',
      };
    
      return (
        <div className="h-screen flex items-center justify-center" style={introStyles}>
          <div className="text-center text-white">
            <h1 className="text-6xl md:text-8xl font-bold mb-4">Masáže Petra</h1>
            <p className="text-lg mb-8 bg-purple-500  p-1">Profesionálne masáže ku vám domov</p>
            <Button onClick={scrollToContact}>Objednajte sa</Button>
          </div>
        </div>
      );
};

export default Header;