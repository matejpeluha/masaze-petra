import React from 'react';

const DownloadLocalFile: React.FC = () => {
    const downloadFile = () => {
        const fileURL = '/certifikat.jpeg';
    
        const link = document.createElement('a');
        link.href = fileURL;
        link.download = 'petra-peluhova-certifikat.jpeg';
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
      };

  return (
    <div className="grow">
      <button
        onClick={downloadFile}
        className="w-full text-center grow py-2 px-4 bg-purple-600 text-white rounded transition duration-300 hover:bg-purple-800"
      >
        Stiahnuť certifikát
      </button>
    </div>
  );
};

export default DownloadLocalFile;
