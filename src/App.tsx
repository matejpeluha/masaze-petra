import React from 'react';
import './App.css';
import Header from './components/Header';
import AboutMe from './components/AboutMe';
import Contact from './components/Contact';

const App: React.FC = () => {
  const scrollToContact = () => {
    const scrollToElement = document.getElementById('contact-section');
    if (scrollToElement) {
      scrollToElement.scrollIntoView({ behavior: 'smooth' });
    }
  };

  return (
    <div className="font-sans text-brownish">
      <Header scrollToContact={scrollToContact} />
      <AboutMe />
      <Contact />
    </div>
  );
};

export default App;
